/**
**    DCL
**    Hotels de Catalunya
**        (Part 2.1)
**
**    Autors: 
**      Marcos Lopez Capitan
**      Raul Delgado Escalona
**      Matthew Alexander Calcagno
**
**
**	Enunciat:
**		Implementa un subprograma que donat un hotel_id i un any ens retorni cert/fals
**		depenen de si l'hotel té o no reserves per aquell any. Cal tenir en compte que hi poden haver
**		reserves per més d'un any.
**      
**    Creacio: 06/05/2019
**
**
*/

USE db_hotels;

DROP FUNCTION IF EXISTS sp_HotelTeReserves;
DELIMITER //
CREATE FUNCTION sp_HotelTeReserves(pHotelId INT, pAny INT) RETURNS VARCHAR(15)

BEGIN
	DECLARE pTimes INT;
	DECLARE vResp VARCHAR(5);
  
    -- Increment primes si la data d'inici o la data fi són iguals l'any introduit
    -- també incrementa si la data inici és més petita i la data fi és més gran que pAny.
    -- (Això últim voldria dir que l'any demanat está dintre del rang de la reserva)
		SELECT count(*) INTO pTimes
		FROM reserves r
        INNER JOIN habitacions ha ON r.hab_id = ha.hab_id
		INNER JOIN hotels ho ON ho.hotel_id = ha.hotel_id
		WHERE YEAR(r.data_inici) = pAny                                       -- si l'inici es igual
          OR YEAR(r.data_fi) = pAny                                    -- si el final es igual
          OR ((YEAR(r.data_inici) < pAny) AND (YEAR(r.data_fi) > pAny));  -- si l'any esta dintre del rang de reserva
    
	-- Si el nombre és més gran que 0 vol dir que hi ha reserves per la qual cosa tornem TRUE
	 IF(pTimes > 0) THEN
		SET vResp = "True";
	 ELSE
     -- Si no troba res tornem False ja que no hi ha registres del HotelID en un any determinat
		SET vResp = "False";
		END IF;
	-- El valor que retorna la funció
	RETURN vResp;

END

//
-- Cridem a la funcio
SELECT sp_HotelTeReserves(1,2014) as Reserva