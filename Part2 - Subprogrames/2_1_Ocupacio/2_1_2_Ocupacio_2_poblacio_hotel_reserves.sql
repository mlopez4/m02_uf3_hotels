/**
**    DCL
**    Hotels de Catalunya
**        (Part 2.1.2)
**
**    Autors: 
**      Marcos López Capitán
**      Raul Delgado Escalona
**      Matthew Alexander Calcagno
**
**
**	Enunciat:
**    Implementa un subprograma que donada una poblacio_id i un any ens retorni el primer
**    hotel (hotel_id) que trobi que no té cap reserva per aquell any.
**                      sp_PoblacioHotelNoTeReserves(pPoblacioId, pAny)
**      
**    Creacio: 03/05/2019
**
**
*/


USE db_hotels;

DROP FUNCTION IF EXISTS sp_PoblacioHotelNoTeReserves ;


DELIMITER //

CREATE FUNCTION sp_PoblacioHotelNoTeReserves(pPoblacioId SMALLINT(5), pAny SMALLINT(4)) RETURNS SMALLINT(5)

BEGIN
DECLARE pHotelId SMALLINT(5);


SELECT h1.hotel_id INTO pHotelId
  FROM hotels h1
WHERE hotel_id NOT IN (SELECT DISTINCT h.hotel_id
                        FROM hotels h
                      INNER JOIN habitacions hab ON h.hotel_id = hab.hotel_id
                      INNER JOIN reserves r ON hab.hab_id = r.hab_id
                      WHERE YEAR(r.data_fi) = pAny
                      AND h.poblacio_id = pPoblacioId
                      AND r.reserva_id IS NOT NULL)
AND h1.poblacio_id = pPoblacioId
LIMIT 1;

RETURN pHotelId;

END
//

SELECT sp_PoblacioHotelNoTeReserves(26,2013) AS HotelID_sense_reserva;


