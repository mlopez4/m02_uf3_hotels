/**
**    DCL
**    Hotels de Catalunya
**        (Part 2.1.3)
**
**    Autors: 
**      Marcos L�pez Capit�n
**      Raul Delgado Escalona
**      Matthew Alexander Calcagno
**
**
**	Enunciat:
**		(1 punt) Implementa un subprograma que donat un hotel_id, un any i opcionalment un mes ens
        retorni el percentatge d'ocupaci� d'aquell hotel en tant per 1 (0.0 fins 1.0) per aquell any i mes. Si
        pMes �s NULL cal que ens retorni l'ocupaci� de tot l'any.
        sp_HotelOcupacio(pHotelId, pAny, pMes)
**      
**    Creacio: 03/05/2019
**
**
*/


/* CONSIDERACIONS
    Considerem que cap reserva agafa dos anys diferents. (Cap reserva passa la nit de final dany a l'hotel)
*/
USE db_hotels;


DROP FUNCTION IF EXISTS sp_HotelOcupacio;
DELIMITER //
CREATE FUNCTION sp_HotelOcupacio(pHotel_id SMALLINT(5), pAny INT, pMes INT) RETURNS FLOAT(3,2)
  BEGIN

    DECLARE ocupacio FLOAT;
    DECLARE capacitatPossibleAny INT;
    DECLARE capacitatPossibleMes INT;

    SELECT count(*)*365 INTO capacitatPossibleAny -- Multiplicant totes les habitacions per 365 dies obtenim tota la capacitat que pot assolir l'hotel demanat en un any
      FROM habitacions
    WHERE hotel_id = pHotel_id;

    SELECT count(*)*30 INTO capacitatPossibleMes -- Tota la capacitat que pot assoir l'hotel demanat en un mes
      FROM habitacions
    WHERE hotel_id = pHotel_id;



    IF pMes IS NULL THEN
      SELECT  SUM(TIMESTAMPDIFF(DAY,data_inici,data_fi)) INTO ocupacio -- total nits ocupades de l'hotel demanat en l'any demanat
        FROM reserves r
        INNER JOIN habitacions h ON h.hab_id = r.hab_id
      WHERE h.hotel_id = pHotel_id
        AND (pAny BETWEEN YEAR(data_inici) AND YEAR(data_fi)); -- si l'any demanat esta a dins de la reserva
      
      SET ocupacio = (1/capacitatPossibleAny) * ocupacio; -- ocupacio en tant per 1 per un any
    ELSE
      
      SELECT  SUM(TIMESTAMPDIFF(DAY,data_inici,data_fi)) INTO ocupacio -- Total nits ocupades de l'hotel demanat en l'any I MES demanat
        FROM reserves r
        INNER JOIN habitacions h ON h.hab_id = r.hab_id
      WHERE h.hotel_id = pHotel_id
          AND (pMes BETWEEN MONTH(data_inici) AND MONTH(data_fi)) -- si l'any demanat esta a dins de la reserva
          AND (pAny BETWEEN YEAR(data_inici) AND YEAR(data_fi)); -- si el mes demanat esta a dins de la reserva
        
        SET ocupacio = (1/capacitatPossibleMes) * ocupacio; -- ocupacio en tant per 1 per un mes
      
    END IF;
    
	
    RETURN ocupacio;
  
  END
//
DELIMITER;


SELECT sp_HotelOcupacio(5, 2014, null);
   
   
   



