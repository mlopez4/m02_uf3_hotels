/**
**    DCL
**    Hotels de Catalunya
**        (Part 2.2.3)
**
**    Autors: 
**      Marcos López Capitán
**      Raul Delgado Escalona
**      Matthew Alexander Calcagno
**
**
**	Enunciat:
**  (1 punt) Millora del subprograma anterior. Volem que l'hotel s'ompli de manera aleat�ria. �s a dir,
** que el subprograma no vagi omplint habitacions de manera seq�encial fins arribar al
** pPercentatge, sin� que s'omplin les habitacions de manera aleat�ria. El nom d'aquest nou
** subprograma ser�: sp_HotelOmplirForatsAleatori
**      
**    Creacio: 18/05/2019
**
**
*/



/* Aquesta funci� ens retorna un hab_id aleatori entre el n�mero habitacions d'un hotel en concret*/

DROP FUNCTION sp_HabIdAleatoria;

DELIMITER //
CREATE FUNCTION sp_HabIdAleatoria(pHotelId SMALLINT(5)) RETURNS VARCHAR(10)
BEGIN

  -- aqu� ficarem la id de l'habitaci� escollida aleat�riament
	DECLARE vHabId MEDIUMINT(8) UNSIGNED;
  
  -- aquest sera el valor aleatori
  DECLARE vValorAleatori INT;
  
  DECLARE fi_cursor            BOOLEAN DEFAULT false;
  DECLARE c1 CURSOR FOR SELECT hab_id
                          FROM habitacions
                        WHERE hotel_id = pHotelId;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET fi_cursor = true;
  
  SELECT FLOOR(RAND()*(
                        SELECT COUNT(*)
                          FROM habitacions
                        WHERE hotel_id = pHotelId
                        ) ) INTO vValorAleatori;
  
                        
  -- fem servir un cursor per mirar quin registre de la taula habitacions est� en la posici� que coincideix amb el nostre valor aleatori
  OPEN c1;
    FETCH c1 INTO vHabId;
    WHILE (fi_cursor = false) DO
      
      -- si el nostre valir aleatori ha arribat a 0, sortim del cursor per retornar la hab_id escollida
      IF vValorAleatori = 0 THEN
        SET fi_cursor = true;
      END IF;
      
      -- decrementem el valor aleatori per al proxim registre
      SET vValorAleatori = vValorAleatori -1;
      FETCH c1 INTO vHabId;
    END WHILE;
  CLOSE c1;
    
  
  RETURN vHabId;
END
//
DELIMITER ;













/*Aquest procediment comprova si estan ocupades totes les dates d'un determinat any una per una en una habitaci�
agafada aleat�riament amb l'ajuda de la anterior funci�*/
DROP PROCEDURE IF EXISTS sp_HotelOmplirForatsAleatori;
DELIMITER //

CREATE PROCEDURE sp_HotelOmplirForatsAleatori(pHotelId SMALLINT(5), pAny SMALLINT(5), pPercentatge FLOAT(3,2))
BEGIN

  -- variables necessaries
  DECLARE fi_cursor BOOLEAN DEFAULT false;
  DECLARE temp_hab MEDIUMINT(8);
  DECLARE temp_percentatge FLOAT (3,2) DEFAULT 0.0;
  DECLARE data_final DATE;
  DECLARE temp_dataActual DATE;
  DECLARE temp_numReserves INT DEFAULT 0;
  DECLARE c1 CURSOR FOR SELECT hab_id
                          FROM habitacions 
                        WHERE hotel_id = pHotelId;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET fi_cursor = true;
  
  -- assignem valors
  SET temp_dataActual = CONCAT(pAny, '/01/01'); -- data que anira incrementant 
  SET  data_final = DATE_ADD(temp_dataActual, INTERVAL 1 YEAR); -- data a on s'ha d'arrivar (final d'any) 
  
  WHILE temp_percentatge < pPercentatge DO
  
    SET temp_hab = sp_HabIdAleatoria(pHotelId);
    
    WHILE (temp_dataActual < data_final) AND (temp_percentatge < pPercentatge)  DO
    
        -- per cada habitacio consultem si la data a consultar (temp_dataAtual) forma part de cap reserva de la habitacio on es troba el cursor
        SELECT COUNT(*) INTO temp_numReserves
          FROM reserves
        WHERE hab_id = temp_hab
              AND temp_dataActual BETWEEN data_inici AND data_fi;
              
        -- si la data actual esta lliure inserim una reserva d'una nit
        IF temp_numReserves = 0 THEN
          INSERT INTO reserves (hab_id, data_inici, data_fi, client_id, data_creacio)
            VALUES(temp_hab, DATE_SUB(temp_dataActual, INTERVAL 1 DAY), temp_dataActual, 10001,NOW());
        END IF;
        
         -- incrementem un dia a la data a comprovar (comprovem el dia seguent) per la seguent itineració
        SET temp_dataActual = ADDDATE(temp_dataActual, 1);
         -- actualitzem el percentatge per comprovar si hem arribat a l'objectiu
        SET temp_percentatge = sp_HotelOcupacio(pHotelId, pAny, null);
        -- tornem a posar el numero de reseves a 0
        SET temp_numReserves = 0;
      
      END WHILE;
    
  
  END WHILE;
  
END
  //
  DELIMITER ;




CALL sp_HotelOmplirForatsAleatori(7, 2014, 0.3);


-- COMPROVACIO
SELECT sp_HotelOcupacio(7, 2014, null);









