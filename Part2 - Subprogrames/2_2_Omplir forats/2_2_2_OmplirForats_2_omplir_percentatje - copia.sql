/**
**    DCL
**    Hotels de Catalunya
**        (Part 2.2.2)
**
**    Autors: 
**      Marcos López Capitán
**      Raul Delgado Escalona
**      Matthew Alexander Calcagno
**
**
**	Enunciat:
**  (1 punt) Implementa un subprograma encarregat d'omplir de reserves un hotel fins un percentatge i
**  un any passats per paràmetre.
**  La signatura del subprograma és el següent:
**                      sp_HotelOmplirForats(pHotelId, pAny, pPercentatge)
**
**  Nota: el pPercentatge serà expressat en tant per 1 (valor entre 0 i 1) indicant el tipus de percentatge (0=0% i 1 = 100%).
**      
**    Creacio: 18/05/2019
**
**
*/


USE db_hotels;

DROP PROCEDURE IF EXISTS sp_HotelOmplirForats;
DELIMITER //

CREATE PROCEDURE sp_HotelOmplirForats(pHotelId SMALLINT(5), pAny SMALLINT(5), pPercentatge FLOAT(3,2))
BEGIN

  -- variables necessaries
  DECLARE fi_cursor BOOLEAN DEFAULT false;
  DECLARE temp_hab MEDIUMINT(8);
  DECLARE temp_percentatge FLOAT (3,2) DEFAULT 0.0;
  DECLARE data_final DATE;
  DECLARE temp_dataActual DATE;
  DECLARE temp_numReserves INT DEFAULT 0;
  DECLARE c1 CURSOR FOR SELECT hab_id
                          FROM habitacions 
                        WHERE hotel_id = pHotelId;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET fi_cursor = true;
  
  -- assignem valors
  SET temp_dataActual = CONCAT(pAny, '/01/01'); -- data que anira incrementant 
  SET  data_final = DATE_ADD(temp_dataActual, INTERVAL 1 YEAR); -- data a on s'ha d'arrivar (final d'any) 
  
  
  OPEN c1;
    FETCH c1 INTO temp_hab;

    -- mentre el cursor no arribi al final i el percentatge no compleixi l'objectiu el cursor estara en funcionament
    WHILE (fi_cursor = false) DO
    
    SET temp_dataActual = CONCAT(pAny, '/01/01'); -- tornem a posar a 1 de gener
    
      -- anem incrementant la data a comprovar en un dia, fins a comprovar un any sencer per cada habitacio (mentre que el percentatge estigui per sota del desitjat)
      WHILE (temp_dataActual < data_final) AND (temp_percentatge < pPercentatge)  DO
    
        -- per cada habitacio consultem si la data a consultar (temp_dataAtual) forma part de cap reserva de la habitacio on es troba el cursor
        SELECT COUNT(*) INTO temp_numReserves
          FROM reserves
        WHERE hab_id = temp_hab
              AND temp_dataActual BETWEEN data_inici AND data_fi;
              
        -- si la data actual esta lliure inserim una reserva d'una nit
        IF temp_numReserves = 0 THEN
          INSERT INTO reserves (hab_id, data_inici, data_fi, client_id, data_creacio)
            VALUES(temp_hab, DATE_SUB(temp_dataActual, INTERVAL 1 DAY), temp_dataActual, 10001,NOW());
        END IF;
        
         -- incrementem un dia a la data a comprovar (comprovem el dia seguent) per la seguent itineració
        SET temp_dataActual = ADDDATE(temp_dataActual, 1);
         -- actualitzem el percentatge per comprovar si hem arribat a l'objectiu
        SET temp_percentatge = sp_HotelOcupacio(pHotelId, pAny, null);
        -- tornem a posar el numero de reseves a 0
        SET temp_numReserves = 0;
      
      END WHILE;
    
      FETCH c1 INTO temp_hab;
    END WHILE;
  CLOSE c1;
  
END
  //
  DELIMITER ;
  
  
-- cridem el mètode
CALL sp_HotelOmplirForats(5,2014,0.5);