/**
**    DCL
**    Hotels de Catalunya
**        (Part 2.2)
**
**    Autors: 
**      Marcos Lopez Capitan
**      Raul Delgado Escalona
**      Matthew Alexander Calcagno
**
**
**	Enunciat:
**  	Implementa un subprograma encarregat d'omplir forats d'un habitació en un any.
**		Donada una habitació i una any, el subprograma ha de buscar entre quines dates no hi ha
**		reserves i inventar-se reserves que omplin aquests forats. Cal que el client que realitza la reserva
**		no sempre sigui el mateix (valor aleatori de la taula clients)..
**      
**    Creacio: 06/05/2019
**
**
*/

USE db_hotels;

DROP PROCEDURE IF EXISTS sp_HabitacioOmplirForats;
DELIMITER //
CREATE PROCEDURE sp_HabitacioOmplirForats(IN pHabId INT, IN pAny INT)
BEGIN
	DECLARE vUser_Rand INT DEFAULT -1;
    DECLARE vDate DATE;
    DECLARE fi_cursor BOOLEAN DEFAULT false;
-- Declaramos el cursor para el SELECT de toda las fechas donde la habitacion ID no tiene una reserva en un año determinado
    DECLARE cur CURSOR FOR SELECT distinct data_inici    
		FROM reserves
		WHERE data_inici not in (select distinct data_inici from reserves WHERE  YEAR(data_inici) = pAny  AND hab_id = pHabId ORDER BY data_inici ASC)
		AND YEAR(data_inici) = pAny
		ORDER BY data_inici ASC;
-- Declaramos un handler para cuando acabe el cursor
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET fi_cursor = true;
-- Comienza el curosr
    OPEN cur;
    FETCH cur INTO vDate;
    WHILE (fi_cursor = false) DO
-- Aqui obtendremos 1 ID de un usuario aleatoriamente
		SELECT client_id FROM clients ORDER BY RAND() LIMIT 1 INTO vUser_Rand;
-- Insertamos a la tabla de reservas la fecha desde el cursor como data_inici y el Id del clente aleatorio       
        INSERT INTO reserves(hab_id,data_inici,data_fi,client_id) VALUES (pHabId,vDate,vDate,vUser_Rand);
-- Fetch para que vaya al siguiente registro
        FETCH cur INTO vDate;
    
    END WHILE;
-- Cerramos el cursor
	CLOSE cur;
    
END
//
-- Llamamos el procedimiento
CALL sp_HabitacioOmplirForats(20699,2014)
//


