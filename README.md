# Hotels i reserves de Catalunya
## Activitat de la UF3 de la assignatura M2 Base de Dades

Alumnes:
*  	Matthew Alexander Calcagno
*  	Raul Delgado Escalona
*  	Marcos López Capitán

Disposem d'una [base de dades](https://github.com/robertventura/databases/tree/master/db_hotels) que guarda
informació relacionada amb les reserves dels
hotels de 4 i 5 estrelles de tot Catalunya.


Partint d'un [enunciat](https://gitlab.com/mlopez4/m02_uf3_hotels/blob/master/ENUNCIAT-Hotels-Subprogrames-Triggers.pdf) proporcionat pel professor hem aplicat una sèrie de canvis en l'estructura i contingut de la base de dades.
El procediment seguit s'ha estructurat mitjançant carpetes:

* [Part 1 Tipus i Preus](https://gitlab.com/mlopez4/m02_uf3_hotels/tree/master/Part1%20-%20Tipus%20i%20preus)
    * [Script](https://gitlab.com/mlopez4/m02_uf3_hotels/blob/master/Part1%20-%20Tipus%20i%20preus/01_01_Creacio%20de%20taules.sql) creació de taules
    * [Script](https://gitlab.com/mlopez4/m02_uf3_hotels/blob/master/Part1%20-%20Tipus%20i%20preus/01_02_Asisignar_tipus_hab_taula_habitacions.sql) assignar a cada habitació amb un tipus d'habitació
    * [Script](https://gitlab.com/mlopez4/m02_uf3_hotels/blob/master/Part1%20-%20Tipus%20i%20preus/01_03_Omplir%20taula%20tarifes.sql) Omplir la taula de tarifes segons la categoria de l'hotel al qual correspon cada habitació
    * [Explicació](https://gitlab.com/mlopez4/m02_uf3_hotels/blob/master/Part1%20-%20Tipus%20i%20preus/01_04_Proposta%20canvi.md) solució a la proposta de millora

* [Part 2 Subprogrames](https://gitlab.com/mlopez4/m02_uf3_hotels/tree/master/Part2%20-%20Subprogrames)
    * Ocupació
        * [Script](https://gitlab.com/mlopez4/m02_uf3_hotels/blob/master/Part2%20-%20Subprogrames/2_1_Ocupacio/2_1_1_Ocupacio_1_reserves_any.sql) per crear un subprograma que donat un hotl_id i un any ens retorna cert o fals depenent de si te reserves aquest any.
        * [Script](https://gitlab.com/mlopez4/m02_uf3_hotels/blob/master/Part2%20-%20Subprogrames/2_1_Ocupacio/2_1_2_Ocupacio_2_poblacio_hotel_reserves.sql) per crear un subprograma que donada una poblacio_id i un any ens retorni el primer hotel que trobi sense cap reserva
        * [Script](https://gitlab.com/mlopez4/m02_uf3_hotels/blob/master/Part2%20-%20Subprogrames/2_1_Ocupacio/2_1_3_Ocupacio_3_percentatge_ocupacio.sql) per crear un subprograma que donat un hotel_id, any i opcionalment un mes ens retorni el percentatge ocupació
    * Omplir Forats
        * [Script](https://gitlab.com/mlopez4/m02_uf3_hotels/blob/master/Part2%20-%20Subprogrames/2_2_Omplir%20forats/2_2_1_OmplirForats_1_habitacio_any.sql) per implementar un subprograma que emplena forats d'una habitació en un any
        * [Script](https://gitlab.com/mlopez4/m02_uf3_hotels/blob/master/Part2%20-%20Subprogrames/2_2_Omplir%20forats/2_2_2_OmplirForats_2_omplir_percentatje%20-%20copia.sql) per implementar un subprograma que emplena les reserves d'un hotel fins a un percentatge i any
        * [Script](https://gitlab.com/mlopez4/m02_uf3_hotels/blob/master/Part2%20-%20Subprogrames/2_2_Omplir%20forats/2_2_3_OmplirForats_3_omplir_aleatoriament%20-%20copia.sql) per implementar un subprograma com l'anterior però de manera aleatòria

* [Part 3 Disparadors](https://gitlab.com/mlopez4/m02_uf3_hotels/tree/master/Part3%20-%20Disparadors)
    * Manteniment Ocupacio
        * [Script](https://gitlab.com/mlopez4/m02_uf3_hotels/blob/master/Part3%20-%20Disparadors/3_1_Manteninemt%20ocupacio/3_1_1_hotels_ocupacio_Crear%20Taula.sql) que crea la taula hotels ocupacio
        * [Script](https://gitlab.com/mlopez4/m02_uf3_hotels/blob/master/Part3%20-%20Disparadors/3_1_Manteninemt%20ocupacio/3_1_2_Inicialitzar_valors_taula.sql) per emplenar la taula anterior amb valors
        * [Script](https://gitlab.com/mlopez4/m02_uf3_hotels/blob/master/Part3%20-%20Disparadors/3_1_Manteninemt%20ocupacio/3_1_3_manteniment.sql) implementar disparadors per mantenir la taula actualitzada
    * Facturació Clients
        * [Script](https://gitlab.com/mlopez4/m02_uf3_hotels/blob/master/Part3%20-%20Disparadors/3_2_Facturacio%20clients/3_2_1_Facturacio%20clients_Crear%20Taula.sql) que crea la taula facturació clients
        * [Script](https://gitlab.com/mlopez4/m02_uf3_hotels/blob/master/Part3%20-%20Disparadors/3_2_Facturacio%20clients/3_2_2_Inicializar%20valors%20-%20copia.sql) per emplenar la taula anterior amb valors
        * [Script](https://gitlab.com/mlopez4/m02_uf3_hotels/blob/master/Part3%20-%20Disparadors/3_2_Facturacio%20clients/3_2_3_manteniment.sql) implementar disparadors per mantenir la taula actualitzada