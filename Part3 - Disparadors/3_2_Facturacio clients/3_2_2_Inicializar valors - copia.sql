/**
**    DCL
**    Hotels de Catalunya
**        (Part 3.2.1)
**
**    Autors: 
**      Marcos L�pez Capit�n
**      Raul Delgado Escalona
**      Matthew Alexander Calcagno
**
**  
**	  Enunciat:
**   Inicialitza el valor de la taula clients_facturacio taula amb les dades existents (ajuda't un subprograma).
**      
**    Creacio: 08/05/2019
**
**
*/


USE db_hotels;











/*
Aquesta funcio ens retorna el preu-nit de la habitacio entrada per parametre.
*/
DROP FUNCTION IF EXISTS sp_preuHabitacio;
DELIMITER // 
CREATE FUNCTION sp_preuHabitacio(pHabitacio INT) RETURNS DECIMAL(6,2) 
BEGIN 
  DECLARE vPreu_nit DECIMAL(6,2); 
  
  SELECT t.preu_nit INTO vPreu_nit 
    FROM habitacions h
    INNER JOIN tarifes_hotels t ON t.hotel_id = h.hotel_id
                                AND t.tipus_id = h.tipus
  WHERE h.hab_id = pHabitacio;
    
  RETURN vPreu_nit; 
END 
//
DELIMITER ;











/*
Aquest procediment comproba a la taula "clients_facturacio" si els valors entrats per parametre
coincideixen amb cap registre ja insertat, si es aix� fa un UPDATE i si NO, fa un INSERT.
*/

DROP PROCEDURE IF EXISTS sp_calcularFacturacioRegistre;
DELIMITER //
CREATE PROCEDURE sp_calcularFacturacioRegistre(IN pClient INT(11), IN pMes INT, IN pAny INT, IN pNumNits INT, IN pTotal DECIMAL(8,2))
BEGIN

  IF (pClient, pMes, pAny) IN (SELECT client_id, mes, any FROM clients_facturacio) THEN 
              
    UPDATE clients_facturacio
      SET num_reserves = num_reserves + 1,
          num_nits = num_nits + pNumNits,
          total_facturat = total_facturat + pTotal
    WHERE client_id = pClient 
          AND mes = pMes 
          AND any = pAny;

    -- si no existeix...
  ELSE    

    INSERT INTO clients_facturacio
    VALUES (pClient, pAny, pMes, 1, pNumNits, pTotal );
                  
  END IF;

END
//
DELIMITER ;













/*
Procediment que calcula la facturacio de un client en una habitcio concreta (la fracciona en anys - mesos)
i la registra a la taula clients_facturacio
(Fa servir les la funcio i el procediment anteriors)
Valorem si el client_id es NULL ja que existeix un registre sense client i en la nostra taula "clients_facturacio"
tenim una FK a la taula "clients"
*/
DROP PROCEDURE IF EXISTS sp_calcularFacturacio;
DELIMITER //
CREATE PROCEDURE sp_calcularFacturacio(IN pReserva INT(10))
BEGIN

  -- declarem les variable necessaries
  DECLARE vClient         INT(11);
  DECLARE vHabitacio      SMALLINT(5);
  DECLARE vDataAnterior   DATE;
  DECLARE vDataActual     DATE;
  DECLARE vDataFi         DATE;
  DECLARE vPreuNit        DECIMAL(6,2);
  DECLARE vPreuAcumulat   DOUBLE(6,2)     DEFAULT 0.0;
  DECLARE vNitsAcumulades INT             DEFAULT 0;
  
  
  -- iniciem les variables amb els valors del registre corresponent a la reserva passada per parametre
  SELECT client_id, hab_id, data_inici, data_fi INTO vClient, vHabitacio, vDataActual, vDataFi
    FROM reserves
  WHERE reserva_id = pReserva;
  
  -- variable d'ajuda per comparar el canvi de mes
  SET vDataAnterior = vDataActual;
  
  -- el preu per nit de la habitacio demanada a la reserva
  SET vPreuNit =  sp_preuHabitacio(vHabitacio);
  
  IF vClient IS NOT NULL THEN

      -- mentre la data temporal sigui mes petita que l'ultima data ...
      WHILE vDataActual <= vDataFi DO
          
            SET vPreuAcumulat = vPreuAcumulat + vPreuNit;
            SET vNitsAcumulades = vNitsAcumulades + 1;
              
            -- Si el mes nou diferent que l'anteriror...
            IF (MONTH(vDataAnterior) != MONTH(vDataActual)) THEN

              CALL sp_calcularFacturacioRegistre(vClient, MONTH(vDataAnterior), YEAR(vDataAnterior), vNitsAcumulades, vPreuAcumulat);
              
              SET vPreuAcumulat = 0.00;
              SET vNitsAcumulades = 0;
                
            END IF;

            SET vDataAnterior = vDataActual;
            SET vDataActual = ADDDATE(vDataActual,1);
     
      END WHILE;
      
      CALL sp_calcularFacturacioRegistre(vClient, MONTH(vDataAnterior), YEAR(vDataAnterior), vNitsAcumulades, vPreuAcumulat);
  
  END IF;

END
//
DELIMITER ;


call sp_calcularFacturacio(4);




/*
Procediment que recorre tots el registres de la taula "reserves" i aplica
el procediment anterior (sp_calcularFacturacio) per omplenar la taula "clients_facturacio"
amb totes les dades que ja tenim.
*/
DROP PROCEDURE IF EXISTS sp_omplenarClients_acturacio;

DELIMITER //
CREATE PROCEDURE sp_omplenarClients_acturacio()
BEGIN

  DECLARE vReserva INT(10);
  
  DECLARE fi_cursor BOOLEAN DEFAULT false;
  DECLARE c1 CURSOR FOR SELECT reserva_id
                            FROM reserves;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET fi_cursor = true;
  
    
    OPEN c1;
      FETCH c1 INTO vReserva;
      WHILE (fi_cursor = false) DO
        
        CALL sp_calcularFacturacio(vReserva);
          
        FETCH c1 INTO vReserva;
      END WHILE;
    CLOSE c1;

END
//
DELIMITER ;


CALL sp_omplenarClients_acturacio;










































