/**
**    DCL
**    Hotels de Catalunya
**        (Part 3.2.1)
**
**    Autors: 
**      Marcos L�pez Capit�n
**      Raul Delgado Escalona
**      Matthew Alexander Calcagno
**
**  
**	  Enunciat:
**   Crea una nova taula nomenada CLIENTS_FACTURACIO a on hi haur� la informaci� de la
**   facturaci� dels nostres clients per any i mes.
**      
**    Creacio: 08/05/2019
**
**
*/





USE db_hotels;
-- Eliminamos la tabla si existe y luego creamos la tabla. El Primary Key sera compuesta por el client_id,
--  any y mes.
DROP TABLE IF EXISTS clients_facturacio;
CREATE TABLE IF NOT EXISTS clients_facturacio(
	  client_id		    INT(11),
    any				      SMALLINT(4) UNSIGNED,
    mes				      SMALLINT(2) UNSIGNED,
    num_reserves	  INT(6) UNSIGNED,
    num_nits		    INT(6) UNSIGNED,
    total_facturat	DECIMAL(8,2),
    
    CONSTRAINT PK_clients_facturacio PRIMARY KEY (client_id,any,mes),
    CONSTRAINT FK_clients_facturacio_clients FOREIGN KEY (client_id) REFERENCES clients(client_id)
);