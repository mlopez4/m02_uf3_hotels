/**
**    DCL
**    Hotels de Catalunya
**        (Part 3.2.3)
**
**    Autors: 
**      Marcos López Capitán
**      Raul Delgado Escalona
**      Matthew Alexander Calcagno
**
**  
**	  Enunciat:
**    Mitjançant triggers volem realitzar el manteniment de la taula clients_facturacio. Tingues en
**    compte a quina o quines taules hauràs de crear els disparadors per tal de dur a terme aquest
**    manteniment automàtic.
**    Ajuda't de subprogrames per reaprofitar codi i fer-ne més fàcil el manteniment
**      
**    Creacio: 13/05/2019
**
**
*/

USE db_hotels;




/*
Disparador que ens mante la taula clients_facturacio actualitzada
Crida el procediment de l'exercici anterior cad vegada que es va un INSERT
*/
DROP TRIGGER trg_mantenimentFacturacio;
  
DELIMITER //
  
  CREATE TRIGGER trg_mantenimentFacturacio AFTER INSERT
    ON reserves FOR EACH ROW
    BEGIN 
    
      CALL sp_calcularFacturacio(NEW.reserva_id);
    
    END//
    
DELIMITER ;














