/**
**    DCL
**    Hotels de Catalunya
**        (Part 3.1.1)
**
**    Autors: 
**      Marcos L�pez Capit�n
**      Raul Delgado Escalona
**      Matthew Alexander Calcagno
**
**
**	Enunciat:
**  	Crea una nova taula anomenada HOTELS_OCUPACIO. A on volem guardar el percentatge
      d'ocupaci� d'un hotel en un mes i any concrets per treure'n estad�stics
**      
**    Creacio: 05/05/2019
**
**
*/



USE db_hotels;

CREATE TABLE hotels_ocupacio(
  hotel_id    SMALLINT(5) UNSIGNED,
  mes         SMALLINT(2) UNSIGNED,
  any         SMALLINT(4) UNSIGNED,
  ocupacio    FLOAT(3,2),
  CONSTRAINT PK_hotels_ocupacio PRIMARY KEY (hotel_id, any, mes),
  CONSTRAINT FK_hotels_ocupacio_hotels FOREIGN KEY (hotel_id) REFERENCES hotels(hotel_id)
);
