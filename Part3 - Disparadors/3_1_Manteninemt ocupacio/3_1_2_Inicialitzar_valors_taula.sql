/**
**    DCL
**    Hotels de Catalunya
**        (Part 3.1.2)
**
**    Autors: 
**      Marcos López Capitán
**      Raul Delgado Escalona
**      Matthew Alexander Calcagno
**
**
**	Enunciat:
**  	Inicialitza el valor de la taula hotels_ocupacio amb les dades existents (ajuda't un subprograma).
**      
**    Creacio: 06/05/2019
**
**
*/


DROP PROCEDURE sp_OmplenarOcupacio;
/*
Procediment que va recorrent tots els registres de la taula "reserves"
i per cada mes de la reserva aplica l'anterior funció que havíem creat per 
emplenar la nova taula "hotels_ocupacio"
*/
DELIMITER //
CREATE PROCEDURE sp_OmplenarOcupacio()
BEGIN
  
  -- variables necessàries
  DECLARE fi_cursor BOOLEAN DEFAULT false;
  DECLARE vInici DATE;
  DECLARE vFinal DATE;
  DECLARE tmp_hotel_id SMALLINT(5) UNSIGNED;
  DECLARE c1 CURSOR FOR SELECT hotel_id
                          FROM hotels;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET fi_cursor = true;
  
  
  OPEN c1;
  
  -- rang de dates a calcular
    SELECT MIN(data_inici), MAX(data_fi) INTO vInici, vFinal
      FROM reserves;
      
    FETCH c1 INTO tmp_hotel_id;
    WHILE (fi_cursor = false) DO
      
      -- es fa el càlcul a cada mes, fins a arribar a l'última data de reserves (Fent servir la funció que ja tenìem creada)
      WHILE (vInici<=vFinal) DO
      
        INSERT INTO hotels_ocupacio
        VALUES (tmp_hotel_id, MONTH(vInici), YEAR(vInici), sp_HotelOcupacio(tmp_hotel_id, YEAR(vInici), MONTH(vInici)));
      
                  
        SET vInici = DATE_ADD(vInici, INTERVAL 1 MONTH);
      
      END WHILE;
      FETCH c1 INTO tmp_hotel_id;
    END WHILE;
  CLOSE c1;
  
    
  
END
//
DELIMITER ;








CALL sp_OmplenarOcupacio();










