/**
**    DCL
**    Hotels de Catalunya
**        (Part 3.1.3)
**
**    Autors: 
**      Marcos López Capitán
**      Raul Delgado Escalona
**      Matthew Alexander Calcagno
**
**
**	Enunciat:
**  	Mitjançant triggers volem realitzar el manteniment de la taula hotels_ocupacio. Tingues en
** compte a quina o quines taules hauràs de crear els disparadors per tal de dur a terme aquest
** manteniment automàtic.
** Ajuda't de subprogrames anteriors per reaprofitar codi i fer-ne més fàcil el manteniment
**      
**    Creacio: 10/05/2019
**
**
*/

DROP TRIGGER trg_mantenimentOcupacio;
  
DELIMITER //
  
  CREATE TRIGGER trg_mantenimentOcupacio AFTER INSERT
    ON reserves FOR EACH ROW
    BEGIN
    
      -- declarem variables necessaries per consultar l'ocupacio
      DECLARE vReserva INT(10);         -- id de la reserva
      DECLARE vHotelId SMALLINT(5);     -- id del hotel
      DECLARE vAny INT;                 -- any on tindra lloc la reserva
      DECLARE vMesInici INT;            -- mes on começa la reserva
      DECLARE vMesFinal INT;            -- ultim mes de la reserva  
      
      -- omplenem variables
      SET vAny = YEAR(new.data_inici);
      SET vMesInici = MONTH(new.data_inici);
      SET vMesFinal = MONTH(new.data_fi);
      SET vReserva = new.reserva_id;
      SELECT hotel_id INTO vHotelId
        FROM habitacions
      WHERE hab_id = new.hab_id;
      
      
      -- per cada mes implicat en la reserva actualitzem el seu registre corresponent a la taula hotels_ocupacio (si no existeix el registre es fa un de nou)
      WHILE vMesInici <= vMesFinal DO
          
          -- si existeix...
          IF (vHotelId, vMesInici, vAny) IN (SELECT hotel_id, mes, any FROM hotels_ocupacio) THEN 
          
            UPDATE hotels_ocupacio
              SET ocupacio = sp_HotelOcupacio(vHotelId, vAny, vMesInici)
            WHERE vHotelId = vHotelId 
              AND mes = vMesInici 
              AND any = vAny;
            
          -- si no existeix...
          ELSE    
          
            INSERT INTO hotels_ocupacio
              VALUES (vHotelId, vMesInici, vAny, sp_HotelOcupacio(vHotelId, vAny, vMesInici));
              
          END IF;
          
        SET vMesInici := vMesInici + 1;
      END WHILE;
        
    END//
    
DELIMITER ;


