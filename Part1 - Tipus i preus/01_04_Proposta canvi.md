
    DCL
    Hotels de Catalunya
        (Part 1.3)

    Autors: 
      Marcos L�pez Capit�n
      Raul Delgado Escalona
      Matthew Alexander Calcagno


	Enunciat:
		El nostre cap ens demana la nostra opini� respecte un altre canvi que ens demanen. Es tracte de
		que en el preu de les habitacions tamb� s'ha de tenir en compte la temporada de l'any que ens
		trobem (ALTA, MITJA, BAIXA). Depenent de la temporada els preus variaran. Quina proposta li
		fer�eu? No cal que la implementis nom�s indica qu� fer�eu.
      
    Creacio: 24/04/2019







La proposta seria construir una altra taula (dates_temporades) on concretar�em els intervals de dates corresponents a cada temporada. Temporada, data inici i data fi.
Seguidament creem una altra taula (hotels_tarifes_temporades) on cada registre seria un hotel amb els corresponents increments de preus en tant per � adient per cada temporada.
Per �ltim fent servir un trigger modificar�em les consultes relacionades amb preus segons la data actual (current_date()) tenint en compte l'id del hotel consultat i la corresponent taula d'increment per temporada.

