/**
**    DCL
**    Hotels de Catalunya
**        (Part 1.1)
**
**    Autors: 
**      Marcos López Capitán
**      Raul Delgado Escalona
**      Matthew Alexander Calcagno
**
**
**	Enunciat:
** En l'esquema de la base de dades hi falta un mecanisme per guardar el preu de cadascuna de les
** habitacions dels diferents hotels.
**  • Cada habitació ha de pertànyer en algun d'aquests tipus (individual, doble d'ús individual,
**      doble, doble + llit supletori, triple, junioar suite, suite i suite nupcial)
**  • Cal afegir una nova taula anomenada tarifes_hotels a on hi ha les tarifes de cada hotel per
**      cada tipus d'habitació.
**  Crea un subprograma en MySQL anomenat sp_AddTarifesSchema encarregat
** 	d'afegir aquests canvis en la base de dades actual.
**      
**    Creació: 24/04/2019
**
**
*/


  USE db_hotels;







/*
  Crea les taules que manquen a l'activitat hotels UF3 Part1 (tipus_habitacions i tarifes_hotels)
*/
DROP PROCEDURE IF EXISTS sp_AddTarifesSchema;
DELIMITER //
CREATE PROCEDURE sp_AddTarifesSchema()
BEGIN

  CREATE TABLE tipus_habitacions (
    tipus_id  INT(2) UNSIGNED,
    nom       VARCHAR(30) NOT NULL,
    CONSTRAINT PK_tipus_habitacions PRIMARY KEY (tipus_id)
  );
  
  -- Afegim els tipus d'habitacions que tenim (Registres)
  INSERT INTO tipus_habitacions
    VALUES(1, 'individual'),
          (2, 'doble d\'us idividual'),
          (3, 'doble'),
          (4, 'doble + llit supletori'),
          (5, 'triple'),
          (6, 'junior suite'),
          (7, 'suite'),
          (8, 'suite nupcial');
  
  
  
  
  
  CREATE TABLE tarifes_hotels (
    hotel_id  SMALLINT(5) UNSIGNED,
    tipus_id  INT(2) UNSIGNED,
    preu_nit  DECIMAL(6,2) NOT NULL,
    CONSTRAINT PK_tarifes_hotels PRIMARY KEY (hotel_id, tipus_id),
    CONSTRAINT FK_tarifes_hotels_hotels FOREIGN KEY (hotel_id)
      REFERENCES hotels(hotel_id),
    CONSTRAINT FK_tarifes_hotels_tipus_habitacions FOREIGN KEY (tipus_id)
      REFERENCES tipus_habitacions(tipus_id)
  );
  
  -- Afegim la columna tipus a la taula habitacions
  ALTER TABLE habitacions
    ADD COLUMN tipus INT(2) UNSIGNED,
    ADD CONSTRAINT FK_habitaions_tipus_habitacions FOREIGN KEY (tipus)
      REFERENCES tipus_habitacions(tipus_id);
  
END
//
DELIMITER ;





CALL sp_AddTarifesSchema();











