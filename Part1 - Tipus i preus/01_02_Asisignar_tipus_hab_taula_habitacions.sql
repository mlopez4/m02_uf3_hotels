/**
**    DCL
**    Hotels de Catalunya
**        (Part 1.2)
**
**    Autors: 
**      Marcos López Capitán
**      Raul Delgado Escalona
**      Matthew Alexander Calcagno
**
**
**	Enunciat:
**		Crea un subprograma en MySQL anomenat sp_AssignarTipusHab encarregat
**      d'assignar de manera aleatòria un tipus d'habitació en cadascuna de les habitacions de la
**      BD que no tenen assignat ja un tipus d'habitació concret.
**      
**    Creacio: 24/04/2019
**
**
*/


  USE db_hotels;








/*
Torna un "hab_id" aleatori de la taula tipus_habitacions
Per fer-ho obtenim un numero aleatori entre 0 i el numero máxim de registres de la taula "tipus_habitacions"

*/
DROP FUNCTION IF EXISTS sp_ValorAleatori;
DELIMITER //
CREATE FUNCTION sp_ValorAleatori() RETURNS INT(2)
BEGIN

  
  DECLARE vValorAleatori INT(2); -- un valor aleatori comprès entre 0 i el numero de registres que tenim
  DECLARE vContador INT(2) DEFAULT 0;
  DECLARE vTipus_id INT(2);
  

  -- Declarem el cursor per recorrer la taula tipus_habitacions
  DECLARE fi_cursor BOOLEAN DEFAULT false;
  DECLARE c1 CURSOR FOR SELECT tipus_id 
                          FROM tipus_habitacions;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET fi_cursor = true;
  
  
  -- Escollim un valor aleatori (nomes representara el numero de
  --  registre, NO el tipus_id!!!)
	SELECT FLOOR(RAND()*(
                        SELECT COUNT(*)
                          FROM tipus_habitacions
                        ) ) INTO vValorAleatori;
                        
  -- obrim el cursor
  OPEN c1;
  FETCH c1 INTO vTipus_id;
  WHILE (fi_cursor = false)  DO
    
    -- si el contador arriba al valor aleatori escollim l'id del tipus dhabitacio
    -- si no passem al seguent registre
    IF (vContador = vValorAleatori) THEN
    
      SET fi_cursor = true;
      
    ELSE 
    
        -- incrementem el contador
      SET vContador = vContador +1;
      FETCH c1 INTO vTipus_id;
      
    END IF;
    
  END WHILE;
  CLOSE c1;
                        
  RETURN vTipus_id;
      
END
//
DELIMITER;











/*
  Procediment que recorre tots els registres de la taula "habitacions" i
        assigna de manera aleatòria un tipus d'habitació en cadascun en el nou 
        camp "tipus" que hem creat en l'exercici anterior.
*/
DROP PROCEDURE IF EXISTS sp_AssignarTipusHab;
DELIMITER //
CREATE PROCEDURE sp_AssignarTipusHab()
BEGIN

  DECLARE vHab_id MEDIUMINT(8);
  DECLARE fi_cursor BOOLEAN DEFAULT false;
  DECLARE c1 CURSOR FOR SELECT hab_id 
                          FROM habitacions;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET fi_cursor = true;
  
  
  OPEN c1;
  FETCH c1 INTO vHab_id;
  WHILE (fi_cursor = false) DO
    
    UPDATE habitacions
		
      SET tipus = sp_ValorAleatori() -- Script per crear la funcio es mes avall
    WHERE hab_id = vHab_id;
    
    FETCH c1 INTO vHab_id;
  END WHILE;
  CLOSE c1;
  
END
//
DELIMITER ;




CALL sp_AssignarTipusHab();
















