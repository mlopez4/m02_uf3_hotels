/**
**    DCL
**    Hotels de Catalunya
**        (Part 1.3)
**
**    Autors: 
**      Marcos L�pez Capit�n
**      Raul Delgado Escalona
**      Matthew Alexander Calcagno
**
**
**	Enunciat:
**		Crea un subprograma en MySQL anomenat sp_OmplirTarifesHotels que ompli la
**		taula TARIFES_HOTELS. Cal que el subprograma sigui coherent amb les dades
**		introdu�des, �s a dir, que una SUITE ha de tenir un preu/nit m�s alt que una INDIVIDUAL.
**      
**    Creacio: 24/04/2019
**
**
*/


  USE db_hotels;


/*
  Ens incrementa un valor passat per par�metre depenent del tipus d'hotel
  trobat a partir del segon par�metre (hotel_id)
    1 estrella incrementa 0%
    2 estrella incrementa 10%
    3 estrella incrementa 20%
    4 estrella incrementa 30%
    5 estrella incrementa 40%
*/
DROP FUNCTION IF EXISTS sp_aplicarIncrement;
DELIMITER //
CREATE FUNCTION sp_aplicarIncrement(pPreu INT, pId SMALLINT(5)) RETURNS DECIMAL(6,2)
BEGIN

  DECLARE vPreu DECIMAL(6,2);
  DECLARE vCategoriaHotel INT(2);
  
  SELECT categoria INTO vCategoriaHotel
    FROM hotels
  WHERE hotel_id = pId;
  
  CASE vCategoriaHotel
    WHEN 1 THEN SET vPreu = pPreu;
    WHEN 2 THEN SET vPreu = pPreu + (pPreu * 0.10);
    WHEN 3 THEN SET vPreu = pPreu + (pPreu * 0.20);
    WHEN 4 THEN SET vPreu = pPreu + (pPreu * 0.30);
    WHEN 5 THEN SET vPreu = pPreu + (pPreu * 0.40);
  END CASE;
    
    RETURN vPreu;
  
END
//
DELIMITER ;









/*
  Omplena la taula tarifes_hotels
  fent servir la funcio sp_aplicarIncrement per canviar el preu
  depenent de la categoria del hotel
    tipus_id 1 preu base 20
    tipus_id 2 preu base 30
    tipus_id 3 preu base 40
    tipus_id 4 preu base 50
    tipus_id 5 preu base 60
    tipus_id 6 preu base 80
    tipus_id 7 preu base 100
    tipus_id 8 preu base 150
  El preu definitiu sincrementara depenent de la categoria del hotel fent servir la funcio "sp_aplicarIncrement"
*/
DROP PROCEDURE IF EXISTS sp_OmplirTarifesHotels;
DELIMITER //
CREATE PROCEDURE sp_OmplirTarifesHotels()
BEGIN

  INSERT INTO tarifes_hotels
    SELECT h.hotel_id, t.tipus_id, CASE t.tipus_id
                                  WHEN 1 THEN sp_aplicarIncrement(20, h.hotel_id)
                                  WHEN 2 THEN sp_aplicarIncrement(30, h.hotel_id)
                                  WHEN 3 THEN sp_aplicarIncrement(40, h.hotel_id)
                                  WHEN 4 THEN sp_aplicarIncrement(50, h.hotel_id)
                                  WHEN 5 THEN sp_aplicarIncrement(60, h.hotel_id)
                                  WHEN 6 THEN sp_aplicarIncrement(80, h.hotel_id)
                                  WHEN 7 THEN sp_aplicarIncrement(100, h.hotel_id)
                                  WHEN 8 THEN sp_aplicarIncrement(150, h.hotel_id)
                                  END AS preu
  FROM hotels h
  INNER JOIN tipus_habitacions t;
  
END
//
DELIMITER ;

CALL sp_OmplirTarifesHotels();


  